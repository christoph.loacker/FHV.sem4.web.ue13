var firstError = "";

function _isTextValid(id){
    if(document.getElementById(id).value == ""){
        if(firstError == "")firstError = id;
        return false;
    }
    return true;
}

function _isRadioValid(name){
    if(form.elements[name].value == ""){
        if(firstError == "")firstError = id;
        return false;
    }
    return true;
}

function _isUsernameValid(id){
    
     if(document.getElementById(id).value.match(/^[A-Za-z_]{5,8}$/) == ""){
        if(firstError == "")firstError = id;
        return false;
    }
    return true;
}

function _isPasswordValid(id, id2){
    var isValidPassword =  (document.getElementById(id).value.match(/^[a-zA-Z][a-zA-Z0-9_]{5,8}$/)!=null);

    var passwordsMatch = (document.getElementById(id).value == document.getElementById(id2).value);
    if(!(isValidPassword && passwordsMatch)){
        if(firstError == "")firstError = id;
        return false;
    }
    return true;
}

function _isEmailValid(id){
    
    if(document.getElementById(id).value.match(/[a-zA-Z]([a-zA-Z0-9_\-].?)*@([a-zA-Z0-9_\-].?)+\.([a-zA-Z0-9_\-].?){2,}/) == ""){
        if(firstError == "")firstError = id;
        return false;
    }
    return true;
}

function _isCheckboxValid(name){
    var elements = document.getElementsByName(name);
    for(i = 0; i < elements.length; i++){
        if(elements[i].checked){
            if(firstError == "")firstError = id;
            return true;
        }
    }
    return false;
}

function checkSubmit(){
    var errorMsg = "";
    errorMsg+= _isTextValid("vorname") ? "" : "Vorname falsch \r\n";
    errorMsg+= _isTextValid("nachname") ? "" : "Nachname falsch \r\n";
    errorMsg+= _isTextValid("adresse") ? "" : "Adresse falsch \r\n";
    errorMsg+= _isTextValid("username") ? "" : "Username falsch \r\n";

    errorMsg+= _isRadioValid("geschlecht") ? "" : "Geschlecht fehlt \r\n";
    errorMsg+= _isRadioValid("studienjahr") ? "" : "Studienjahr fehlt \r\n";
    errorMsg+= _isUsernameValid("username") ? "" : "Username ungültig \r\n";
    errorMsg+= _isPasswordValid("password", "password2") ? "" : "Passwort ungültig \r\n";
    errorMsg+= _isEmailValid("email") ? "" : "Email ungültig \r\n";
    errorMsg+= _isCheckboxValid("berechtigung") ? "" : "Berechtigungen fehlen\r\n"

    if(errorMsg!=""){
        alert(errorMsg);
        document.getElementById(firstError).focus();
        return false;
    }else{
        window.location="mailto:admin@example.at";
        window.setTimeout(2000);
        return true;
    }
  
}
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
    <head>
        <script src="https://code.jquery.com/jquery-latest.js"></script>
        <link rel="stylesheet" href="mainStyle.css">
        <link rel="stylesheet" href="print.css">
        <title>Kursübersicht</title>
        <meta charset="utf-8">
        <script src="script.jss"></script>
        
        <style>
            form{
                padding:5px;
            }
            input {
                margin:5px;
            }
            
            textarea{
                margin:5px;
                height:100px;
                width:300px;
            }
            
        </style>
    </head>
    <body>
        <form name="form" action="controller" method="GET" onsubmit="return checkSubmit()">
            <input type="text" id="vorname" name="vorname" placeholder="Vorname"><br/>
            <input type="text" id="nachname" name="nachname" placeholder="Nachname"><br/>
            <textarea id="adresse" name="adresse" placeholder="Adresse"></textarea><br/>
            <label>Land: </label>
            <select name="land">
                <option>Österreich</option>
                <option>Deutschland</option>
                <option>Schweiz</option>
                <option>Andere</option>
            </select><br/>
            <input type="text" id="username" name="username" placeholder="Username"><br/>
            <input type="password" name="password1" id="password" placeholder="Password"><br/>
            <input type="password" name="password2" id="password2" placeholder="Password bestätigen"><br/>
            <label>Geschlecht: </label>
            <input type="radio" id="geschlechtM" name="geschlecht" value="maennlich">
            <label for="geschlechtM"> Männlich </label> 
            <input type="radio" id="geschlechtW" name="geschlecht" value="weiblich">
            <label for="geschlechtW"> Weiblich</label><br/>
            <label>Studiengang:</label>
            <select>
                <option>ITB2</option><option>ITB4</option>
                <option>ITB6</option>
                <option>Andere</option>
            </select><br/>
            
            <input type="radio" id="studienjahr17" name="studienjahr" value="2017">
            <label for="studienjahr17"> 2017 </label> 
            <input type="radio" id="studienjahr18" name="studienjahr" value="2018">
            <label for="studienjahr18"> 2018</label>
            
            <input type="radio" id="studienjahr19" name="studienjahr" value="2019">
            <label for="studienjahr19"> 2019</label>
            <br/>
            
            <label>Berechtigungen: </label><br>
            <input type="checkbox" value="Admin" name="berechtigung" value="admin"> Admin
            <input type="checkbox" value="Dozent" name="berechtigung" value="dozent"> Dozent
            <input type="checkbox" value="Student" name="berechtigung" value="student"> Student
            <br>
            <input type="text" name="email" placeholder="Email" id="email"><br>
            <input type="submit"><input type="reset">
            <input type="hidden" name="dispatchto" value="saveNewUser">
        </form>
    </body>
</html>
<html>
    <head>
        <link rel="stylesheet" href="mainStyle.css">
        <link rel="stylesheet" href="print.css">
        <title>Kursübersicht</title>
        <meta charset="utf-8">
        <meta name="description" content="Kurse FHV">
        <meta name="keywords" content="Kurse, FHV">
        <meta name="author" content="Christoph Loacker">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="language" content="de-de">
        
    </head>
    <body>
        <h1>Aktuelle Kurse</h1>
        <ul>
            <li><a href="kursMathe.html">Mathe</a></li>
            <li><a href="kursEnglisch.html">Englisch</a></li>
            <li><a href="kursDeutsch.jsp">Deutsch</a></li>
            <li><a href="kursTurnen.html">Turnen</a></li>
        </ul>
        <h3><a href="userIDBeantragung.jsp">Registrieren</a></h1>
    </body>
</html>